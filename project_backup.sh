#!/bin/bash
now=$(date +"%m_%d_%Y")
yest=$(date -d "yesterday" +"%m_%d_%Y")
project_dir=$1
project_name=$2
db_type=$3
db_name=$4
db_user=$5
db_pass=$6
db_host=${7}
git_branch=${8}
git_message=${9}
git_origin=${10}
force=${11}
backup_folder=${12}
parammetters=${13}
error=false
errorMessage=""
shoudlBackup=true

if [ -z "$git_origin" ] || [ $git_origin -lt 1 ]
then
    git_origin="origin"
fi
if [ -z "$git_message" ] || [ $git_message -lt 1 ]
then
    git_message="Project backup"
fi
if [ -z "$git_branch" ] || [ $git_branch -lt 1 ]
then
    git_branch="master"
fi
if [ -z "$db_host" ] || [ $db_host -lt 1 ]
then
    db_host="localhost"
fi

cd $project_dir


gitStatus=$(git status)
if [[ "$gitStatus" == *"nothing to commit"* ]] && [ $force -lt 1 ]; then
  echo "Not changes found";
  shoudlBackup=false
else

  if [ $db_type == 'mysql' ]
  then
    echo "MySql db selected"

    mysqldump  -h $db_host -u $db_user -p$db_pass $db_name  >  $backup_folder/$project_name-dump-$now.sql

    filesize=$(stat -c%s "$project_dir/$backup_folder/$project_name-dump-$now.sql")
    if [ $filesize -lt 1 ]
    then
        error=true
        errorMessage=$errorMessage" - Couldn't create mysql dump"
    else
        rm $backup_folder/$project_name-dump-$yest.sql
    fi
  elif [ $db_type == 'mongo' ]
  then
    echo "Mongo db selected"

    mongodump -h $db_host -d $db_name -o $backup_folder/$project_name-dump-$now
    zip -r $project_name-dump-$now.zip $backup_folder/$project_name-dump-$now

    filesize=$(stat -c%s "$project_dir/$backup_folder/$project_name-dump-$now")
    if [ $filesize -lt 1 ]
    then
        error=true
        errorMessage=$errorMessage" - Couldn't create mongo zip file"
    else
        mv $project_name-dump-$now.zip $backup_folder
        rm -rf $backup_folder/$project_name-dump-$now
        rm -rf $backup_folder/$project_name-dump-$yest.zip
    fi
  elif [ $db_type == 'postgresql' ]
  then
    echo "PostgreSQL db selected"

    pg_dump -U $db_user $db_name > $backup_folder/$project_name-dump-$now.dmp

    filesize=$(stat -c%s "$project_dir/$backup_folder/$project_name-dump-$now.dmp")
    if [ $filesize -lt 1 ]
    then
        error=true
        errorMessage=$errorMessage" - Couldn't create postgresql dmp file"
    else
        rm $backup_folder/$project_name-dump-$yest.dmp
    fi  
  elif [ $git_origin != "0" ]
  then
    errorMessage="Unknown db type selected"
    echo $errorMessage
  fi

  git add -A
  git commit -m "$git_message $now"
  git push $git_origin $git_branch
  gitStatus=$(git status)
  if echo "$gitStatus" | grep -q "nothing to commit"; then
    echo "Successfull push";
  else
    error=true
    errorMessage=$errorMessage" - Failed to push changes into repo: "$gitStatus
  fi

fi

if $shoudlBackup;
then
  if $error; then
      echo "Error > "$error
      echo "Error Message > "$errorMessage
  else 
      echo "Backup success."
  fi
  php -r "$(curl -s https://bitbucket.org/developer-office/project_backup/raw/master/sendMail.php)" $parammetters $project_name $git_branch $errorMessage
fi