require_once "Mail.php";

$from = '<' . $argv[1] . '>';
$to = '<' . $argv[2] . '>';
$username = $argv[3];
$password = $argv[4];
$project = $argv[5];
$enviroment = $argv[6];
$isError = $argv[7];
$subject = $project . ' - ' . $enviroment . ' - Project backup - ' . date("d-m-Y") . ' - ' . ($isError ? 'ERROR' : 'SUCCESS');

if (isset($_SERVER['HTTP_CLIENT_IP'])) $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if (isset($_SERVER['HTTP_X_FORWARDED'])) $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if (isset($_SERVER['HTTP_FORWARDED'])) $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if (isset($_SERVER['REMOTE_ADDR'])) $ipaddress = $_SERVER['REMOTE_ADDR'];
else $ipaddress = 'UNKNOWN';

$headers = array(
    'From' => $from,
    'To' => $to,
    'Subject' => $subject
);

$smtp = Mail::factory('smtp', array(
    'host' => 'ssl://smtppro.zoho.eu',
    'port' => '465',
    'auth' => true,
    'username' => $username,
    'password' => $password
));

$errorMessage = array_splice($argv, 7);

$body = $isError ? "There was an error during the backup from '$project' with IP $ipaddress\nError log:\n" . implode(" ", $errorMessage) : "Backup success from '$project' with IP $ipaddress.";

$mail = $smtp->send($to, $headers, $body);

if (PEAR::isError($mail)) {
    echo $mail->getMessage();
} else {
    echo "Message sent: " . $body;
}